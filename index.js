const express = require("express");
const app = express();
const path = require("path");
const  { v4 : uuidv4 } = require('uuid');


app.get("/", (req, res) => {
  res.send("Hi vishal kumar");
});

app.get("/html", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "introduction.html"));
});

app.get("/json", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "sample.json"));
});

app.get("/uuid", (req, res) => {
  let currentUUID = uuidv4()
  res.send(`uuid: ${currentUUID}`)
});

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
